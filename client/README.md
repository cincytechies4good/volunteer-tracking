# client

## Prerequisites
Node v10

To make managing Node versions easy, you can use nvm: 
MacOS/Linux: https://github.com/creationix/nvm#installation-and-update
Windows: https://github.com/coreybutler/nvm-windows

Reference the readme for the appropriate nvm version for how to install Node.js v10.
It should probably look something like this:
```
nvm install 10.12.0
nvm use 10
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your end-to-end tests
```
yarn run test:e2e
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
