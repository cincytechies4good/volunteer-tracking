import axios from 'axios';

const SERVER_URL = 'http://localhost:9000';

const instance = axios.create({
  baseURL: SERVER_URL,
  timeout: 1000
});

export default {
  getAll: () =>
    instance.get('organizations', {
      transformResponse: [data => (data ? JSON.parse(data).content : data)]
    }),
  createNew: name =>
    instance.post('organizations', {
      name,
      description: 'from Vue'
    })
};
