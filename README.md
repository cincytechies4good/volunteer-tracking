# volunteer-tracking

## Overview

Various non-profit organizations that we have engaged with have a common software request. They need a platform which allows them to post events and track volunteer sign-ups. On top of that, they would like the opportunity to keep past volunteers engaged and active. It would also serve as a platform for volunteers to track the hours that they have accumulated, and to get information about the event they are signing up for.

We envision the following core features:
- Social sign in to make registration and log in easy
- Flexible event scheduling for organizations so they can easily post opportunities
- Volunteer registration tracking on an event or role basis
- Location information with google maps integration on events
- “Check in” capability for organizations to track volunteer attendance
- Email/SMS reminders for upcoming events or event updates
- Volunteer features like volunteer hour history and opportunity recommendations
- Requesting help from prior volunteers including email outreach
- The ability to upload informational documents to events or organizations
- The ability to share an event or organization through permalink or social media post

If executed correctly this would be a centrally hosted SaaS solution which many non profit organizations could use.


## Dev Documentation

The core technology stack is:
- [Vue.js](https://vuejs.org/v2/guide/)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [Liquibase](https://www.liquibase.org/documentation/index.html)
- [PostgreSQL](https://www.postgresql.org/about/)

For more technical information such as how to get your environment set up, view the READMEs in the `client` and `server` folders.
- [client/README.md](client/README.md)
- [server/README.md](server/README.md)

If you'd like to get involved, current tasks will be listed on our issue board. Pull one of those in, create a branch or a fork of the repo and then submit a merge request when the feature is complete. For information on how to contribute see [CONTRIBUTING.md](CONTRIBUTING.md)
