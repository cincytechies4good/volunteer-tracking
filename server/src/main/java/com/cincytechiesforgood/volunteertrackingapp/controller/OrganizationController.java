package com.cincytechiesforgood.volunteertrackingapp.controller;

import com.cincytechiesforgood.volunteertrackingapp.exception.ResourceNotFoundException;
import com.cincytechiesforgood.volunteertrackingapp.model.Event;
import com.cincytechiesforgood.volunteertrackingapp.model.Organization;
import com.cincytechiesforgood.volunteertrackingapp.model.User;
import com.cincytechiesforgood.volunteertrackingapp.repository.EventRepository;
import com.cincytechiesforgood.volunteertrackingapp.repository.OrganizationRepository;
import com.cincytechiesforgood.volunteertrackingapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/organizations")
public class OrganizationController {

    private final OrganizationRepository organizationRepository;
    private final EventRepository eventRepository;
    private final UserRepository userRepository;

    @Autowired
    public OrganizationController(OrganizationRepository organizationRepository, EventRepository eventRepository, UserRepository userRepository) {
        this.organizationRepository = organizationRepository;
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
    }

    @GetMapping
    public Page<Organization> getOrganizations(Pageable pageable) {
        return organizationRepository.findAll(pageable);
    }

    @GetMapping("/{organizationId}/events")
    public Page<Event> getAllEventsByOrganization(@PathVariable (value = "organizationId") Long organizationId,
                                              Pageable pageable) {
        return eventRepository.findByOrganizationId(organizationId, pageable);
    }

    @PostMapping("/{organizationId}/events")
    public Event createEvent(@PathVariable (value = "organizationId") Long organizationId,
                                 @Valid @RequestBody Event event) {
        return organizationRepository.findById(organizationId).map(organization -> {
            event.setOrganization(organization);
            return eventRepository.save(event);
        }).orElseThrow(() -> new ResourceNotFoundException("OrganizationId " + organizationId + " not found"));
    }

    @PostMapping("{organizationId}/users")
    public User createUser(@PathVariable (value = "organizationId") Long organizationId,
                           @Valid @RequestBody User user) {
        return organizationRepository.findById(organizationId).map(organization -> {
            user.getOrganizations().add(organization);
            return userRepository.save(user);
        }).orElseThrow(() -> new ResourceNotFoundException("OrganizationId " + organizationId + " not found"));
    }

    @PostMapping
    public Organization postOrganization(@Valid @RequestBody Organization organization) {
        return organizationRepository.save(organization);
    }

}
