package com.cincytechiesforgood.volunteertrackingapp.controller;

import com.cincytechiesforgood.volunteertrackingapp.exception.ResourceNotFoundException;
import com.cincytechiesforgood.volunteertrackingapp.model.Event;
import com.cincytechiesforgood.volunteertrackingapp.model.Organization;
import com.cincytechiesforgood.volunteertrackingapp.model.User;
import com.cincytechiesforgood.volunteertrackingapp.repository.OrganizationRepository;
import com.cincytechiesforgood.volunteertrackingapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;

    @Autowired
    UserController(UserRepository userRepository, OrganizationRepository organizationRepository) {
        this.userRepository = userRepository;
        this.organizationRepository = organizationRepository;
    }

    @GetMapping
    public Page<User> getUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @GetMapping("/{userId}/organizations")
    public Page<Organization> getAllOrganizationsByUser(@PathVariable(value = "userId") Long userId,
                                                  Pageable pageable) {
        return organizationRepository.findAllByUsers_Id(userId, pageable);
    }
}
