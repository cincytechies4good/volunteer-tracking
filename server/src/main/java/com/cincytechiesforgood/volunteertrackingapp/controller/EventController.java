package com.cincytechiesforgood.volunteertrackingapp.controller;

import com.cincytechiesforgood.volunteertrackingapp.model.Event;
import com.cincytechiesforgood.volunteertrackingapp.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/events")
public class EventController {

    @Autowired
    private EventRepository eventRepository;

    @GetMapping
    public Page<Event> getEvents(Pageable pageable) {
        return eventRepository.findAll(pageable);
    }

    @PostMapping
    public Event postEvent(@Valid @RequestBody Event event) {
        return eventRepository.save(event);
    }
}
