package com.cincytechiesforgood.volunteertrackingapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "organization")
public class Organization extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;
    private String description;
    private String address;
    private String city;
    private String state;
    @Column(name="postal_code")
    private String postalCode;
    @Column(name="phone_number")
    private String phoneNumber;
    private String email;
    private String website;
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "organizations")
    private Set<User> users = new HashSet<>();
}
