package com.cincytechiesforgood.volunteertrackingapp.repository;

import com.cincytechiesforgood.volunteertrackingapp.model.Organization;
import com.cincytechiesforgood.volunteertrackingapp.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
    Page<Organization> findAllByUsers_Id(Long userId, Pageable pageable);
}
