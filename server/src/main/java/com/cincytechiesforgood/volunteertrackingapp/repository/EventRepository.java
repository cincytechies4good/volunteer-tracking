package com.cincytechiesforgood.volunteertrackingapp.repository;

import com.cincytechiesforgood.volunteertrackingapp.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Page<Event> findByOrganizationId(Long organizationId, Pageable pageable);
}
